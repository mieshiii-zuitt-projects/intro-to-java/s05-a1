package b137.delacruz.s05a1;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        System.out.println("Phonebook");

        // Instance of Phonebook
        Phonebook pb = new Phonebook();

        //Contact object 1
        Contact firstPerson = new Contact("09191234567", "Tarlac, Philippines");
        //Contact object 2
        Contact secondPerson = new Contact("09201234567", "Tarlac, Philippines");
        //Set name
        firstPerson.setName("Juan Dela Cruz");
        //Set addresses
        firstPerson.setAddresses("Manila, Philippines");
        //Set numbers
        firstPerson.setNumbers("09161234567");



        //Set name
        secondPerson.setName("Pedro Dela Cruz");
        //Set addresses
        secondPerson.setAddresses("Manila, Philippines");
        //Set numbers
        secondPerson.setNumbers("09151234567");

        //Add contacts to Phonebook
        pb.setContact(firstPerson);
        pb.setContact(secondPerson);


        System.out.println("==================================");

        if(pb.getContact().size() == 0){
            System.out.println("Phonebook is empty.");
        } else {

            for(int i = 0; i < pb.getContact().size(); i++){
                System.out.println(pb.getContact().get(i).getName());
                System.out.println("--------------");
                System.out.println(pb.getContact().get(i).getName() + " has the following registered numbers:");
                System.out.println(pb.getContact().get(i).getNumbers());
                System.out.println("----------------------------------");
                System.out.println(pb.getContact().get(i).getName() + " has the following registered addresses:");
                System.out.println(pb.getContact().get(i).getAddresses());
                System.out.println("==================================");

            }
        }

    }

}

