package b137.delacruz.s05a1;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Contact {

    // name of data - type String
    private String name = new String();
    // numbers - type String
    private ArrayList<String> numbers = new ArrayList<String>();
    // addresses - type String
    private ArrayList<String> addresses = new ArrayList<String>();

    //Empty Constructor
    public Contact() {};

    //Parameterized Constructor
    public Contact(String newNumber, String newAddress) {
        this.numbers.add(newNumber);
        this.addresses.add(newAddress);
    }

    //Getters
    //nameGetter
    public String getName() {
        return this.name;
    }
    //numbersGetter
    public String getNumbers() {
        return this.numbers.stream().map(Object :: toString).collect(Collectors.joining("\n"));
    }
    //addressesGetter
    public String getAddresses() {
        return this.addresses.stream().map(Object :: toString).collect(Collectors.joining("\n"));
    }

    //Setters
    //nameSetter
    public void setName(String newName) {
        this.name = newName;
    }
    //numbersSetter
    public void setNumbers(String newNumber) {
        this.numbers.add(newNumber);
    }
    //addressesSetter
    public void setAddresses(String newAddress) {
        this.addresses.add(newAddress);
    }


}
