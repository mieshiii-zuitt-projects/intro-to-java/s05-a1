package b137.delacruz.s05a1;

import java.util.ArrayList;

public class Phonebook {

    //contacts ArrayList
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    //Empty Constructor
    public Phonebook() {};
    //Parameterized Constructor
    public Phonebook(Contact newContact) {
        this.contacts.add(newContact);
    }

    //Getters
    //phonebookGetter
    public ArrayList<Contact> getContact() {
        return this.contacts;
    }


    //Setters
    //phonebookSetter
    public void setContact(Contact newContact) {
        this.contacts.add(newContact);
    }
}
